<?php

namespace Gmlo\CMS\Commands;

use Gmlo\CMS\Modules\Articles\Article;
use Gmlo\CMS\Modules\Categories\Category;
use Gmlo\CMS\Modules\Users\User;
use Gmlo\CMS\Providers\CMSServiceProvider;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Validator;
use File;
use Schema;

class CreateModule extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cms:createmodule';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Module of CMS.';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Creación de modulo para CMS');

        //Artisan::call('vendor:publish');
        //$this->info(__DIR__.'/..');
        $this->asistentecrearmodulo();

        $this->info('Finalizado, adios!!!');
    }

    protected function Lunes()
    {
        $this->info('Por que dios?! , por que?!');
    }

    protected function NoLunes()
    {
        $this->info('uff, por lo menos no es lunes');
    }

    protected function asistentecrearmodulo()
    {
        $data = [];
        $data['nombremodulo'] = $this->ask('Cual es el nombre del modulo? (Escribelo en singular)');
        if(File::exists(__DIR__.'/../views/'.$data['nombremodulo']))
        {
            $this->info('El nombre de modulo ya existe. Intente de nuevo con otro nombre');
        }
        else
        {
            $data['nombremodulo']=strtolower($data['nombremodulo']);
            $data['nombremodulomayus']=ucfirst($data['nombremodulo']);
            $data['pnombremodulo']=$this->aplural(strtolower($data['nombremodulo']));
            $data['pnombremodulomayus']=$this->aplural(ucfirst($data['nombremodulo']));
            if (Schema::hasTable($data['nombremodulo'])===false)
            {
                if($this->crearmodulo($data)==true)
                {
                    $this->info('Modulo creado exitosamente!!!, Para poder utilizar la tabla debes usar el comando migrate de artisan');
                }
                else
                {
                    $this->info('Ocurrio un error al crear el modulo');
                }
            }
            else
            {
                $this->info('Ya existe una tabla con el nombre del modulo especificado');
            }
        }
        
    }  

    protected function crearmodulo($datos)
    {
        $_directorio_principal=$this->creardirectorio_principal($datos);
        $_requests=$this->crear_requests($datos);
        $_modules=$this->crear_modules($datos);
        $_crear_migrations=$this->crear_migrations($datos);
        $_crear_language=$this->crear_language($datos);
        $_crear_controllers=$this->crear_controllers($datos);
        //$_crear_routes=true;
        $_crear_routes=$this->crear_routes($datos);
        $_crear_enlace_menu=$this->crear_enlace_menu($datos);
        if($_directorio_principal==true&&$_requests==true&&$_modules==true&&$_crear_migrations==true&&
            $_crear_language==true&&$_crear_controllers==true&&$_crear_routes==true&&$_crear_enlace_menu==true)
            {
                return true;
            }    
        return false;
    }

    protected function crear_enlace_menu($datos)
    {
        if(File::append(__DIR__.'/../views/partials/items_menu_lateral.blade.php',
                "{!! CMS::makeLinkForSidebarMenu('CMS::admin.".$datos['pnombremodulo'].".index', trans('CMS::".$datos['pnombremodulo'].".".$datos['pnombremodulo']."'), 'fa fa-file') !!}")!==false)
        {
            return true;
        }
        return false;
    }

    protected function crear_routes($datos)
    {
        if(File::put(__DIR__.'/../routes/'.$datos['nombremodulomayus'].'Routes.php',
            "<?php
            Route::resource('".$datos['pnombremodulo']."', '".$datos['pnombremodulomayus']."Controller');")!==false)
        {
            return true;
        }
        return false;
    }

    protected function crear_controllers($datos)
    {
        if(File::put(__DIR__.'/../Controllers/'.$datos['pnombremodulomayus'].'Controller.php', $this->copiarContenido('Controller', $datos['nombremodulo']))!==false)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    protected function crear_language($datos)
    {
        if(File::put(__DIR__.'/../lang/en/'.$datos['pnombremodulo'].'.php',$this->copiarContenido('lang', $datos['nombremodulo']))!==false)
        {
            return true;
        }
        return false;
    }

    protected function crear_migrations($datos)
    {
        if(File::put(__DIR__.'/../stubs/'.$datos['pnombremodulo'].'.stub',$this->copiarContenido('migrations', $datos['nombremodulo']))!==false)
        {
            if(File::put(__DIR__.'/../../../../../database/migrations/'.date('y_m_d').'_000000_create_cms_'.$datos['pnombremodulo'].'_table.php',$this->copiarContenido('migrations', $datos['nombremodulo']))!==false)
            {
                return true;
            }
        }
        return false;
    }

    protected function crear_modules($datos)
    {
        if(File::makedirectory(__DIR__.'/../Modules/'.$datos['pnombremodulomayus'],0777)==1)
        {
            if(File::put(__DIR__.'/../Modules/'.$datos['pnombremodulomayus'].'/'.$datos['nombremodulomayus'].'.php', $this->copiarContenido('Modules', $datos['nombremodulo']))!==false)
            {
                if(File::put(__DIR__.'/../Modules/'.$datos['pnombremodulomayus'].'/'.$datos['nombremodulomayus'].'Presenter.php', $this->copiarContenido('ModulesPresenter', $datos['nombremodulo']))!==false)
                {
                    if(File::put(__DIR__.'/../Modules/'.$datos['pnombremodulomayus'].'/'.$datos['pnombremodulomayus'].'Repo.php', $this->copiarContenido('ModulesRepo', $datos['nombremodulo']))!==false)
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    protected function crear_requests($datos)
    {
        if(File::put(__DIR__.'/../Requests/Create'.$datos['nombremodulomayus'].'.php', $this->copiarContenido('CreateRequest', $datos['nombremodulo']))!==false)
        {
            if(File::put(__DIR__.'/../Requests/Update'.$datos['nombremodulomayus'].'.php', $this->copiarContenido('UpdateRequest', $datos['nombremodulo']))!==false)
            {
                return true;
            }
        }
        return false;
    }

    protected function creardirectorio_principal($datos)
    {
        if(File::makedirectory(__DIR__.'/../views/'.$datos['pnombremodulo'],0777)==1)
        {
            if(File::put(__DIR__.'/../views/'.$datos['pnombremodulo'].'/index.blade.php', $this->copiarContenido('index.blade', $datos['nombremodulo']))!==false)
            {
                if(File::put(__DIR__.'/../views/'.$datos['pnombremodulo'].'/edit.blade.php', $this->copiarContenido('edit.blade', $datos['nombremodulo']))!==false)
                {
                    if(File::put(__DIR__.'/../views/'.$datos['pnombremodulo'].'/create.blade.php', $this->copiarContenido('create.blade', $datos['nombremodulo']))!==false)
                    {
                        if(File::makedirectory(__DIR__.'/../views/'.$datos['pnombremodulo'].'/partials',0777)==1)
                        {
                            if(File::put(__DIR__.'/../views/'.$datos['pnombremodulo'].'/partials/inputs.blade.php', $this->copiarContenido('inputs.blade', $datos['nombremodulo']))!==false)
                            {
                                if(File::put(__DIR__.'/../views/'.$datos['pnombremodulo'].'/partials/scripts.blade.php', $this->copiarContenido('scripts.blade', $datos['nombremodulo']))!==false)
                                {
                                    return true;
                                }                                   
                            }                
                        }
                    }
                }
            }
        } 
        return false;
    }

    private function copiarContenido($tipo, $nombreModulo)
    {
        $plural=$this->aplural($nombreModulo);
        $contenido="";
        switch ($tipo) 
        {
            case 'Controller':
                $contenido = File::get(__DIR__.'/../molds/controller.txt');
                break;
            case 'lang':
                $contenido = File::get(__DIR__.'/../molds/lang.txt');
                break;
            case 'Modules':
                $contenido = File::get(__DIR__.'/../molds/module.txt');
                break;
            case 'ModulesPresenter':
                $contenido = File::get(__DIR__.'/../molds/modulepresenter.txt');
                break;
            case 'ModulesRepo':
                $contenido = File::get(__DIR__.'/../molds/modulerepo.txt');
                break;
            case 'CreateRequest':
                $contenido = File::get(__DIR__.'/../molds/createrequest.txt');
                break;
            case 'UpdateRequest':
                $contenido = File::get(__DIR__.'/../molds/updaterequest.txt');
                break;
            case 'create.blade':
                $contenido = File::get(__DIR__.'/../molds/create.blade.txt');
                break;
            case 'edit.blade':
                $contenido = File::get(__DIR__.'/../molds/edit.blade.txt');
                break;
            case 'index.blade':
                $contenido = File::get(__DIR__.'/../molds/index.blade.txt');
                break;
            case 'inputs.blade':
                $contenido = File::get(__DIR__.'/../molds/inputs.blade.txt');
                break;
            case 'scripts.blade':
                $contenido = File::get(__DIR__.'/../molds/scripts.blade.txt');
                break;
            case 'migrations':
                $contenido = File::get(__DIR__.'/../molds/table.txt');
                break;           
        }
        $contenido = str_replace("articles", lcfirst($plural), $contenido);
        $contenido = str_replace("Articles", ucfirst($plural), $contenido);
        $contenido = str_replace("article", lcfirst($nombreModulo), $contenido);
        $contenido = str_replace("Article", ucfirst($nombreModulo), $contenido);        
        return $contenido;
    }

    public function aplural($cadena)
    {
        $muestra=substr($cadena, -1);
        if($muestra=="a"||$muestra=="e"||$muestra=="i"||$muestra=="o"||$muestra=="u")
        {
            return $cadena."s";
        }
        else
        {
            return $cadena."es";
        }
    }
}