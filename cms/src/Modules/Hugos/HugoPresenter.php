<?php

namespace Gmlo\CMS\Modules\Hugos;

use Gmlo\CMS\Modules\Lib\Presenter;

class HugoPresenter extends Presenter
{
    public function isPublish()
    {
        return $this->published_at != null;
    }

}