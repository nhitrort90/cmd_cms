<?php

namespace Gmlo\CMS\Modules\Hugos;

use Gmlo\CMS\Modules\Lib\BaseRepo;


class HugosRepo extends BaseRepo
{

    public function getModel()
    {
        return new Hugo;
    }

    public function prepareData($data = [])
    {
        $data['created_by'] = \Auth::user()->id;
        return $data;
    }

}